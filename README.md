The module extends views blocks with the ability to be render by the block lazy
Builder.

How to proceed:
- activate the module (need big_pipe, views, blocks)
- In the block layout (or all block selection), instead of using the native
"views (list)" block, use the "with lazy Builder" version.

And it's all.

Note : the real good idea will be to push this feature to core, by adding a
check box "render with lazy builder" to views display configuration.
