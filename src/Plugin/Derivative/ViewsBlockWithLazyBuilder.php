<?php

namespace Drupal\views_block_with_lazy_builder\Plugin\Derivative;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\views\Plugin\Derivative\ViewsBlock;

/**
 * Extends native view Block derivative to find ALL blocks.
 */
class ViewsBlockWithLazyBuilder extends ViewsBlock {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {

    parent::getDerivativeDefinitions($base_plugin_definition);

    foreach ($this->derivatives as $id => $data) {
      $this->derivatives[$id]['category'] = 'Views with LazyBuilder';
      $this->derivatives[$id]['admin_label'] .= ' ' . $this->t('with LazyBuilder');
    }

    return $this->derivatives;
  }

}
