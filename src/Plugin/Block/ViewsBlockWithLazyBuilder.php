<?php

namespace Drupal\views_block_with_lazy_builder\Plugin\Block;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockManager;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\views\Element\View;
use Drupal\views\Plugin\Block\ViewsBlock;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\views\ViewExecutableFactory;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an example block.
 *
 * @Block(
 *   id = "views_bloc_with_lazy_builder_example",
 *   admin_label = @Translation("Views Block with lazy builder"),
 *   category = @Translation("Views Block with lazy builder"),
 *   deriver = "Drupal\views_block_with_lazy_builder\Plugin\Derivative\ViewsBlockWithLazyBuilder"
 * )
 */
class ViewsBlockWithLazyBuilder extends ViewsBlock implements TrustedCallbackInterface {

  /**
   * {@inheritdoc}
   */
  public function build() {

    $build = [];

    $build['lazy_builder_time'] = [
      '#lazy_builder' => [
        static::class . '::viewsBlockWithLazyBuilderCallback', [
          'settings' => Json::encode($this->getConfiguration())
        ],
      ],
      '#create_placeholder' => TRUE,
    ];

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return [
      'viewsBlockWithLazyBuilderCallback',
    ];
  }

  /**
   * Lazy Builder which load views block and serve it.
   *
   * @param string $settings
   *   Jsonized Configuration.
   *
   * @return array
   *   Render array.
   */
  public static function viewsBlockWithLazyBuilderCallback($settings) {

    // Decode settings.
    $config = json::decode($settings);

    // Get Block manager.
    $block_manager = \Drupal::service('plugin.manager.block');

    // Load current block.
    $plugin_block = $block_manager->createInstance($config['id'], $config);

    // Some blocks might implement access check.
    $access_result = $plugin_block->access(\Drupal::currentUser());

    // Return empty render array if user doesn't have access.
    // $access_result can be boolean or an AccessResult class
    if (is_object($access_result) && $access_result->isForbidden() || is_bool($access_result) && !$access_result) {
      // You might need to add some cache tags/contexts.
      return [];
    }

    // Use parent Build to render bloc as native.
    $render = $plugin_block->parentBuild();

    // Add the cache tags/contexts.
    \Drupal::service('renderer')->addCacheableDependency($render, $plugin_block);

    // Return view build.
    return $render['view_build'];
  }

  /**
   * Create helper to call viewsBlock native build.
   *
   * @return array|mixed
   *   Render array.
   */
  public function parentBuild() {
    return parent::build();
  }

}
